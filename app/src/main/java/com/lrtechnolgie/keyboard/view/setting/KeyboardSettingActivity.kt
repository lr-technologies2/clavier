package com.lrtechnolgie.keyboard.view.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.lrtechnolgie.keyboard.R
import com.lrtechnolgie.keyboard.data.KeyboardPreferences
import com.lrtechnolgie.keyboard.data.KeyboardPreferences.Companion.KEY_ENABLE_SOUND
import com.lrtechnolgie.keyboard.data.KeyboardPreferences.Companion.KEY_ENABLE_VIBRATION
import com.lrtechnolgie.keyboard.data.KeyboardPreferences.Companion.KEY_NEEDS_RELOAD
import com.lrtechnolgie.keyboard.databinding.ActivitySettingBinding
import com.lrtechnolgie.keyboard.view.dialog.CustomDialog


class KeyboardSettingActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySettingBinding
    private lateinit var preferences: KeyboardPreferences
    private var enabledKeyboard: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)
        preferences = KeyboardPreferences(applicationContext)

        setupActions()
    }

    override fun onResume() {
        super.onResume()
        checkKeyboardEnabled()
    }

    private fun checkKeyboardEnabled() {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).let {
            enabledKeyboard = it.enabledInputMethodList.any { it.packageName == packageName }
            val dialogShown = supportFragmentManager.findFragmentByTag(CustomDialog.TAG) != null
            if (!enabledKeyboard && !dialogShown) {
                // Show setting dialog
                showEnableKeyboardDialog()
            }

            // Check if the keyboard has been picked
            val imeSelected = Settings.Secure.getString(
                contentResolver,
                Settings.Secure.DEFAULT_INPUT_METHOD
            ).contains(packageName)

            if (enabledKeyboard && !imeSelected) {
                Handler().postDelayed({ it.showInputMethodPicker() }, 500)
            }
        }
    }

    private fun showEnableKeyboardDialog() {
        CustomDialog.Builder()
            .title("Enable keyboard")
            .message("Please enable the LR Keyboard in the settings")
            .positiveText("OK")
            .listener(object : CustomDialog.Listener {
                override fun onPositiveSelect(dialog: CustomDialog) {
                    dialog.dismiss()
                    startActivity(Intent(Settings.ACTION_INPUT_METHOD_SETTINGS))
                }

                override fun onNegativeSelect(dialog: CustomDialog) {
                    dialog.dismiss()
                }
            })
            .build()
            .show(supportFragmentManager, CustomDialog.TAG)
    }


    private fun setupActions() {
        binding.enableVibration.apply {
            isChecked = preferences.getBoolean(KEY_ENABLE_VIBRATION)
            setOnClickListener {
                isChecked = !isChecked
                preferences.putBoolean(KEY_ENABLE_VIBRATION, isChecked)
                preferences.putBoolean(KEY_NEEDS_RELOAD, true)
            }
        }

        binding.enableSound.apply {
            isChecked = preferences.getBoolean(KEY_ENABLE_SOUND)
            setOnClickListener {
                isChecked = !isChecked
                preferences.putBoolean(KEY_ENABLE_SOUND, isChecked)
                preferences.putBoolean(KEY_NEEDS_RELOAD, true)
            }
        }

    }

}